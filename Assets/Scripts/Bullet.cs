﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Bullet : MonoBehaviour
{
    private Transform hitTransform;

    public GameObject explosionParticle;
    
    public int damageRadius = 4;
    
    void Start()
    {
        
    }

    void Update()
    {
        Vector3 vel = GetComponent<Rigidbody2D>().velocity;
        
        Vector3 diff = (transform.position - vel) - transform.position;
        diff.Normalize();
 
        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 180);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        // Explode
        if (other.collider.tag == "Tilemap")
        {
            Vector3 pos = new Vector3(other.GetContact(0).point.x, other.GetContact(0).point.y, 0);

            Instantiate(explosionParticle, pos, quaternion.identity);

            other.collider.GetComponent<DestroyTerrain>().TerrainExplode(pos, damageRadius);

            Destroy(gameObject);
        }
    }
}
