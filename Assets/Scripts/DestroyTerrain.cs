﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using Tile = UnityEngine.WSA.Tile;

public class DestroyTerrain : MonoBehaviour
{
    private Tilemap tilemap;
    public TileBase tile;

    public void Start()
    {
        tilemap = GetComponent<Tilemap>();
    }

    public void TerrainExplode(Vector3 explosionLocation, float radius)
    {
        GetComponent<TilemapCollider2D>().enabled = false;
        
        for (int x = -(int)radius; x < radius; x++)
        {
            for (int y = -(int)radius; y < radius; y++)
            {
                Vector3Int tilePos = tilemap.WorldToCell(explosionLocation + new Vector3(x*.1f, y*.1f, 0));

                if (new Vector2(x, y).magnitude <= radius)
                {
                    if (tilemap.GetTile(tilePos))
                    {
                        DestroyTile(tilePos);
                    }
                }
            }
        }
        
        GetComponent<TilemapCollider2D>().enabled = true;
    }

    void DestroyTile(Vector3Int pos)
    {
        tilemap.SetTile(pos, null);
    }

    public void DrawTile(Vector3Int pos)
    {
        tilemap.SetTile(pos, tile);
    }
}
