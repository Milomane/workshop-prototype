﻿using System;
using System.Collections;
using System.Collections.Generic;
using CollabProxy.UI;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public CharacterController2D controller;

    public float maxBoost;
    public float boost;
    private float horizontalMvt = 0f;
    private bool jump;
    
    // Start is called before the first frame update
    void Start()
    {
        boost = maxBoost;
    }

    // Update is called once per frame
    void Update()
    {
        horizontalMvt = Input.GetAxisRaw("Horizontal");
        if (Input.GetButton("Jump"))
        {
            jump = true;
        }

        if (boost < maxBoost && !jump)
            boost += Time.deltaTime * 0.5f;
        if (boost < maxBoost && controller.m_Grounded)
            boost += Time.deltaTime * 1.5f;
    }

    void FixedUpdate()
    {
        if (controller.m_Grounded && boost > 0f)
        {
            if (jump)
                boost -= Time.deltaTime;
            controller.Move(horizontalMvt/2, false, jump);
        }
        else if (!controller.m_Grounded && boost > 0f)
        {
            if (jump)
                boost -= Time.deltaTime;
            controller.Move(horizontalMvt/3, false, jump);
        }
        jump = false;
    }
}
