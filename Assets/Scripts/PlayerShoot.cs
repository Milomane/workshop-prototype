﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public GameObject bullet;

    public Vector2 target;
    public float force;

    public Transform spawnTransform;
    
    void Update()
    {
        Vector2 positionOnScreen = Camera.main.WorldToViewportPoint (transform.position);
        Vector2 mouseOnScreen = (Vector2)Camera.main.ScreenToViewportPoint(Input.mousePosition);
        float angle = AngleBetweenTwoPoints(positionOnScreen, mouseOnScreen);
        transform.rotation =  Quaternion.Euler (new Vector3(0f,0f,angle));
        
        
        
        if (Input.GetMouseButtonDown(1))
        {
            GameObject obj = Instantiate(bullet, spawnTransform.position, Quaternion.identity);
            
            obj.transform.rotation = Quaternion.Euler (new Vector3(0f,0f,angle - 180));
            obj.GetComponent<Rigidbody2D>().AddForce((mouseOnScreen - positionOnScreen).normalized * force, ForceMode2D.Impulse);
            
            Destroy(obj, 10);
        }
    }
    
    float AngleBetweenTwoPoints(Vector3 a, Vector3 b) {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }
}
