﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UIElements;
using Tile = UnityEngine.WSA.Tile;

public class TileMapManager : MonoBehaviour
{
    public Texture2D groundSprite;

    public Tilemap tilemap;
    public TileBase ground;
    
    public int mapHeight;
    public int mapWidth;

    public float otherPar;
    public float noiseScale;
    public float spawnLimit;
    
    private int xTest = 0;
    private int yTest = 0;

    void Start()
    {
        GenerateRandomTerrain();
    }

    void Update()
    {
        //if (Input.GetMouseButtonDown(0))
        //{
            //GenerateRandomTerrain();
        //}
    }

    public void GenerateRandomTerrain()
    {
        tilemap.ClearAllTiles();
        
        float xRandomOffset = Random.Range(-5000000, 5000000);
        float yRandomOffset = Random.Range(-5000000, 5000000);
        

        // Noise map
        for (int x = 0; x < mapWidth; x++)
        {
            for (int y = 0; y < mapHeight; y++)
            {
                float sampleX = (float)x / (float)mapWidth * (float)noiseScale;
                float sampleY = (float)y / (float)mapHeight * (float)noiseScale;
                float value = Mathf.PerlinNoise((float)sampleX + (float)xRandomOffset, (float)sampleY + (float)yRandomOffset);

                Debug.Log(value + "   " + (float)sampleX + "   " + (float)sampleY + "   " + x + "   " + y);
                
                
                if (value > spawnLimit)
                {
                    tilemap.SetTile(new Vector3Int(x - mapWidth/2, y - mapHeight/2, 0), ground);
                }
            }
        }
    }
}
